PHP Role
=========

This role setup php repository on Ubuntu/Debian server.

Example Playbook
=========
```yml
- hosts: all
  become: true
  roles:
    - php_repository
```
